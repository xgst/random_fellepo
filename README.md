# RANDOM nemBÁNKITÓ FELLÉPŐ GENERÁTOR 

---

honlap:  
http://semmitnembankito2020.herokuapp.com/  
Kb 30 másodpercet várni kell amíg felébred a szerver, utána megy gyorsan :)

---

Példa:  

`
Compact Nightmare Nyers>>

A "háttérsugárzás és a édes" nevű kísérletező szólóprojektből ismert Flipper Csámpás a konzervatóriumból való kicsapása után indította útjára a Compact Nightmare Nyers nevű formációt. Eszeveszett zenei világával apránként felhívta magára a figyelmet. Stílusa riffközpontú keveréke a fizetős blues funky és avantgard pop trash műfajoknak, de mindenkit lenyűgözve egy kis súlytó jazz vagy éppen őrületes soul is fűszerezi monotonnak korántsem mondható zenei alkotásait.

¶~¶~¶

A főként hosszas sorbanállás ihlette punk elemek egymásra hatásából építkező, valamint friss dalszerkezetekkel és erősen modulált elszállásokkal operáló zenészre nagy hatással volt a korai deszkás trap drum and bass archaikus hangzásvilága. Az "Hack Modern Full" című első kislemezdal, illetve a hozzá készült átfogó self-made videoklip több neo-vandáloknak szóló lejátszási listában is szerepelt.

¶~¶~¶

Az elmúlt években frissen facsart transzcendentális glitch rap előadok mellett töltötte meg a performatív klubokat. A lemezről csupa jót írtak a letisztult neo-vandáloknak szóló oldalak, a számok a mai napig előfordulnak európai és tengerentúli webrádiók playlistjein, az előadó pedig turnézott Vatikánban, Örméynországban és Albániában, viszont Bánkon még soha nem játszott. Most újra fog·

×~×~×
`